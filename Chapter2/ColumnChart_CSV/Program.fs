﻿// To run F# Interactive, do SelectAll (CTRL+A) and press ALT+Enter
#load "FSharpChart.fsx"

open System 
open System.IO
open MSDN.FSharp.Charting

let treatLine (line: string) =
    let stringParts = line.Split(';')
    DateTime.Parse stringParts.[0], int stringParts.[1]

let data =
    File.ReadAllLines (__SOURCE_DIRECTORY__ + "\\mydata.txt")    
    |> Array.map treatLine
    
FSharpChart.Column data